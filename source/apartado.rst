Preparación de solución RTK
===========================

Base
++++

En la creación de la fuente de datos geolocalizados se usó un dispositivo piksy para obtener puntos por medio de la técnica RTK.

La base debe ser colocada siguiendo las instrucciones del fabricante: `Swift <https://docs.swiftnav.com/wiki/Piksi_User_Getting_Started_Guide#GPS_RTK_Position/>`_. dejando la antena conectada al chip de la base encima de un trípode de modo que tenga la vista despejada en un ángulo de 30º a la redonda.

.. image:: https://docs.swiftnav.com/w/images/5/53/Antenna1.png
	:width: 1200px
	:height: 800px
	:scale: 50 %
	:alt: alternate text
	:align: center

Una vez fija la base, se debe setear en sus configuraciones la ubicación exacta en longitud y latitud.

Rover
+++++

Para tener actualizaciones de la ubicación actual con margen de error centimétrico, deben haber al menos 2 dispositivos uno de base y uno en movimiento para hacer por RTK el cálculo de posición.

En el caso del Rover, conectarlo por el puerto de carga del dispositivo Android.

Dentro darle permiso a Archaeology de conectarse con el Rover.

Una vez en la zona de Georreferenciación, en un tiempo promedio de 10 minutos se empiezan a recibir actualizaciones de ubicación altamente precisas.
