App
===

Descargar Mapa
++++++++++++++

Aquí se descarga el mapa de la zona de trabajo para facilitar la ubicación durante una jornada.

Se descargarán los datos de la zona definida dentro del cuadrado.


.. image:: _static/Descargarmapa.png
	:width: 600px
	:height: 800px
	:scale: 50 %
	:alt: alternate text
	:align: center

Puede acercarse o alejarse por medio de los botones o haciendo el gesto respectivo con los dedos.
Una vez definida el área, presionar el botón de descarga.

.. image:: _static/down.png
	:alt: alternate text

El mapa se almacena en tiles.

Cargar Archivo de Puntos
++++++++++++++++++++++++

Se pueden cargar puntos de recorridos generados en jornadas previas.

Los puntos a cargar deben estar en archivos tipo GEOJson.

Los datos cargados van a aparecer en el mapa.

.. image:: _static/cargar.png
	:width: 600px
	:height: 800px
	:scale: 50 %
	:alt: alternate text
	:align: center


Georefenciar
++++++++++++

Esta opción permite el registro del recorrido durante la jornada, el cuál por defecto se registra por distancia cada metro.

.. image:: _static/time.png
	:alt: alternate text

Al hacer clic en este botón aparecen opciones de registro de recorrido, que son los siguientes:

+ Por tiempo
+ Por distancia

.. figure:: _static/options.png
	:width: 600px
	:height: 800px
	:scale: 50 %
	:alt: alternate text
	:align: center
	
	Selección entre tipos de registro


Al seleccionar una de las opciones aparecerán los rangos disponibles

.. list-table:: 

	* - .. figure:: _static/distselect.png
		:width: 600px
		:height: 800px
		:scale: 50 %
	
		Selección por distancias	
	
	* - .. figure:: _static/timeselect.png
		:width: 600px
		:height: 800px
		:scale: 50 %

		Selección por tiempo


Ajustes
+++++++

Aquí se selecciona el tipo de Geolocalización a usar: RTK (conectándo a piksi) o GPS

.. figure:: _static/adjustements.png
	:width: 600px
	:height: 800px
	:scale: 50 %
	:alt: alternate text
	:align: center
	
	Ajustes


Enviar
++++++

Aquí se pueden enviar los archivos de de la jornada por el correo electrónico disponible en el dispositivo (de preferencia Gmail).


.. list-table:: 

	* - .. figure:: _static/mail1.png
		:width: 600px
		:height: 800px
		:scale: 50 %	
	
	* - .. figure:: _static/mail2.png
		:width: 600px
		:height: 800px
		:scale: 50 %

	* - .. figure:: _static/mail3.png
		:width: 600px
		:height: 800px
		:scale: 50 %







